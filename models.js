const { Sequelize, DataTypes } = require('sequelize');

// Database connection
const sequelize = new Sequelize('Toko_On', 'postgres', 'Mojito123', {
    dialect: 'postgres',
    host: 'localhost',
    port: 5432,
  });

// Define models
const User = sequelize.define('User', {
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  role: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

const Customer = sequelize.define('Customer', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  address: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

const Item = sequelize.define('Item', {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  price: {
    type: DataTypes.FLOAT,
    allowNull: false
  }
});

const Order = sequelize.define('Order', {
  quantity: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  status: {
    type: DataTypes.STRING,
    allowNull: false,
    //defaultValue: 'pending' // Set default value to 'pending'
  }
});

Order.belongsTo(Customer, { foreignKey: 'customerId' });

module.exports = { User, Customer, Item, Order, sequelize };
