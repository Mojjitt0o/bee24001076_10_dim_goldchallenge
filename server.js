const express = require('express');
const bodyParser = require('body-parser');
const { sequelize } = require('./models');
const endpoints = require('./endpoints');

const app = express();
const PORT = 3000;

// Middleware
app.use(bodyParser.json());

// Use endpoints
app.use('/', endpoints);

// Sync models with database
sequelize.sync()
  .then(() => {
    console.log('Database synced');
    // Start server
    app.listen(PORT, () => {
      console.log(`Server berjalan di port ${PORT}`);
    });
  })
  .catch(err => {
    console.error('Error syncing database:', err);
  });
