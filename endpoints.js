const express = require('express');
const { User, Customer, Item, Order } = require('./models');

const router = express.Router();

// Endpoint registrasi user
router.post('/register', async (req, res) => {
  try {
    const { username, password, role } = req.body;
    const newUser = await User.create({ username, password, role });
    res.status(201).json({ message: 'Registrasi berhasil', user: newUser });
  } catch (error) {
    res.status(500).json({ message: 'Registrasi gagal', error });
  }
});


// Endpoint Login
router.post('/login', async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ where: { username, password } });
    if (user) {
      res.status(200).json({ message: 'Login berhasil', user });
    } else {
      res.status(401).json({ message: 'Username atau password salah' });
    }
  } catch (error) {
    res.status(500).json({ message: 'Error logging in', error });
  }
});


// Endpoint menambahkan customer baru
router.post('/customer', async (req, res) => {
  try {
    const { name, email, address } = req.body;
    const newCustomer = await Customer.create({ name, email, address });
    res.status(201).json({ message: 'Customer berhasil ditambahkan', customer: newCustomer });
  } catch (error) {
    res.status(500).json({ message: 'Gagal menambahkan customer', error });
  }
});


// Endpoint menambahkan item baru
router.post('/item', async (req, res) => {
  try {
    const { name, price } = req.body;
    const newItem = await Item.create({ name, price });
    res.status(201).json({ message: 'Item berhasil ditambahkan', item: newItem });
  } catch (error) {
    res.status(500).json({ message: 'Gagal menambahkan item', error });
  }
});


// Endpoint menambahkan orderan baru dengan status 'Pesanan di proses'
router.post('/order', async (req, res) => {
  try {
    const { customerId, itemId, quantity } = req.body;
    
    // Validasi data
    if (!customerId || !itemId || !quantity) {
      return res.status(400).json({ message: 'Harap lengkapi semua bidang' });
    }
    
    const order = await Order.create({ customerId, itemId, quantity, status: 'Pesanan di proses' });
    res.status(201).json({ message: 'Order diproses', order });
  } catch (error) {
    res.status(500).json({ message: 'Gagal order', error });
  }
});


//Edpoint menampilkan semua data order
router.get('/order', async (req, res) => {
  try {
    const orders = await Order.findAll();
    res.status(200).json({ orders });
  } catch (error) {
    res.status(500).json({ message: 'Gagal menampilkan orders', error });
  }
});


// Endpoint untuk memperbarui status pesanan menjadi selesai
router.put('/order/:orderId', async (req, res) => {
  try {
    const { orderId } = req.params;
    const { status } = req.body;

    // Temukan pesanan berdasarkan ID
    const order = await Order.findByPk(orderId);

    if (!order) {
      return res.status(404).json({ message: 'Pesanan tidak ditemukan' });
    }

    // Perbarui status pesanan
    order.status = status;
    await order.save();

    res.status(200).json({ message: 'Pesanan sudah selesai', order });
  } catch (error) {
    res.status(500).json({ message: 'Gagal memperbarui status pesanan', error });
  }
});


// Endpoint untuk menghapus pesanan
router.delete('/order/:orderId', async (req, res) => {
  try {
    const { orderId } = req.params;

    // Temukan pesanan berdasarkan ID
    const order = await Order.findByPk(orderId);

    if (!order) {
      return res.status(404).json({ message: 'Pesanan tidak ditemukan' });
    }

    const deletedOrderId = orderId;

    // Hapus pesanan
    await order.destroy();

    res.status(200).json({ message: `Pesanan dengan ID ${deletedOrderId} berhasil dihapus`});
  } catch (error) {
    res.status(500).json({ message: 'Gagal menghapus pesanan', error });
  }
});

//Endpoint menampilkan semua data user yang Registrasi
router.get('/registers', async (req, res) => {
  try {
    const registers = await User.findAll();
    res.status(200).json({ registers });
  } catch (error) {
    res.status(500).json({ message: 'Gagal menampilkan User', error });
  }
});


// Endpoint untuk menghapus User
router.delete('/registers/:userId', async (req, res) => {
  try {
    const { userId } = req.params;

    // Temukan pesanan berdasarkan ID
    const user = await User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ message: 'Item tidak ditemukan' });
    }

    const deletedUserId = userId;

    // Hapus pesanan
    await user.destroy();

    res.status(200).json({ message: `User dengan ID ${deletedUserId} berhasil dihapus`});
  } catch (error) {
    res.status(500).json({ message: 'Gagal menghapus item', error });
  }
});


//Endpoint menampilkan semua data customer yang sudah ditambahkan
router.get('/customers', async (req, res) => {
  try {
    const customers = await Customer.findAll();
    res.status(200).json({ customers });
  } catch (error) {
    res.status(500).json({ message: 'Gagal menampilkan customers', error });
  }
});


// Endpoint untuk menghapus User
router.delete('/customers/:customerId', async (req, res) => {
  try {
    const { customerId } = req.params;

    // Temukan pesanan berdasarkan ID
    const customers = await Customer.findByPk(customerId);

    if (!customers) {
      return res.status(404).json({ message: 'Customer tidak ditemukan' });
    }

    const deletedCustomerId = customerId;

    // Hapus pesanan
    await customers.destroy();

    res.status(200).json({ message: `Customer dengan ID ${deletedCustomerId} berhasil dihapus`});
  } catch (error) {
    res.status(500).json({ message: 'Gagal menghapus customer', error });
  }
});


//Endpoint menampilkan semua data item
router.get('/items', async (req, res) => {
  try {
    const items = await Item.findAll();
    res.status(200).json({ items });
  } catch (error) {
    res.status(500).json({ message: 'Gagal menampilkan items', error });
  }
});


// Endpoint untuk menghapus item
router.delete('/items/:itemId', async (req, res) => {
  try {
    const { itemId } = req.params;

    // Temukan pesanan berdasarkan ID
    const item = await Item.findByPk(itemId);

    if (!itemId) {
      return res.status(404).json({ message: 'Item tidak ditemukan' });
    }

    const deletedItemId = itemId;

    // Hapus pesanan
    await item.destroy();

    res.status(200).json({ message: `Item dengan ID ${deletedItemId} berhasil dihapus`});
  } catch (error) {
    res.status(500).json({ message: 'Gagal menghapus item', error });
  }
});

module.exports = router;
